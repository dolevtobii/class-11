#include "threads.h"

void I_Love_Threads()
{
	std::cout << "I love threads" << std::endl;
}

void call_I_Love_Threads()
{
	std::thread t1(I_Love_Threads);

	t1.join();
}

void printVector(std::vector<int> primes)
{
	int i = 0;

	for(i = 0; i < primes.size(); i++)
	{
		std::cout << primes[i] << std::endl;
	}
}


void getPrimes(int begin, int end, std::vector<int>& primes)
{
	int i = 0;
	int j = 0;

	for (i = begin; i <= end; i++) 
	{
		if (isPrime(i) == true)
		{
			primes.push_back(i);
		}
		
	}
}

std::vector<int> callGetPrimes(int begin, int end)
{
	clock_t t;
	t = clock();
	std::vector<int> primes;
	std::thread t1(getPrimes, begin, end, ref(primes));

	t1.join();
	t = clock() - t;
	std::cout << "the time it took to calc: " << (double)t/CLOCKS_PER_SEC << " sec"<< std::endl;
	return primes;
}

void writePrimesToFile(int begin, int end, std::ofstream& file)
{
	int i = 0;
	int j = 0;

	if (file.is_open())
	{
		for (i = begin; i <= end; i++)
		{
			if (isPrime(i) == true)
			{
				file << i;
				file << "\n";
			}
		}
		file.close();
	}
}

void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N)
{
	std::ofstream file(filePath);
	std::thread* arr = new std::thread[N];
	int temp = (end - begin) / N;
	int i = 0;

	for (i = 1; i < N; i++, begin += temp)
	{
		arr[i - 1] = std::thread(writePrimesToFile, begin, end, ref(file));
	}
	arr->join();
}


bool isPrime(int n)
{
	bool isPrime = true;
	if (n > 1)
	{
		for (int i = 2; i <= n / 2 && isPrime != false; i++)
		{
			if (n % i == 0) 
			{
				isPrime = false;
			}
		}
	}
	else
	{
		isPrime = false;
	}
	return isPrime;
}
